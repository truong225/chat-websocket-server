import React from 'react';

export default class ItemUser extends React.Component {
    render() {
        
        return (
            <div>
                <div className="avatar_user_name"></div>
                <div className="user_name">
                    {this.props.name}
                </div>
            </div>
        );

    }
}