import React from 'react';
import MessageList from './MessageList';
import Input from './Input';
import User from '../user/User';
import './AppMes.css';
import Head from './../Head';
import SocketConnection from '../SocketConnection'
import Stomp from '@stomp/stompjs'
import SockJS from 'sockjs-client'

export default class App extends React.Component {
	constructor(props) {
		super(props);

		//Khởi tạo state,
		this.state = {
			open: false,
			nameUserChat: null,
			user: this.props.user,
			messages:[
				{'userId': 'admin', 'message': 'Hello everyone'}
			],
			socket:null
		}
	}

	handleConnectionError(error) {
		alert('WebSocket error: ' + error);
	}

	// Override this method
	componentDidMount(){
		let newMessage=this.state.messages;
		const that=this;
		this.socket=Stomp.over(new SockJS('http://localhost:8080/ws'));
		this.socket.connect({}, function connect(frame){
			this.subscribe("/public/chat", function handle(payload){
				const json=JSON.parse(payload.body);
				newMessage.push(
					{userId: json.userId,
					message: json.message}
				);
				that.setState({messages: newMessage});
			});
		}, this.handleConnectionError);
	}

	sendMessage(m) {
		if (m.value) {
			this.socket.send('/app/chat/sendMessage',{},JSON.stringify({
				"userId": SocketConnection.currentUser,
				"message": m.value
			}));
			m.value = "";
		}
	}
	render() {
		return (
			<div className="app__content">
				<Head />
				<div className="row">
					<div className="col-sm-4">
						<User ref='userName' />
					</div>
					<div className="col-sm-8">
						<div className="chat_window">
							{/* <MessageList messages={SocketConnection.messages} /> */}
							<MessageList messages={this.state.messages} />
							<Input
								ref='inputChat'
								sendMessage={this.sendMessage.bind(this)}
							/>
						</div>
					</div>
				</div>
			</div>
		)
	}
}