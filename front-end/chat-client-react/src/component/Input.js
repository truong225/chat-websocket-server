import React from 'react';

export default class Input extends React.Component {
    constructor(props){
        super(props);
        this.state={
            textChat:''
        }
    }

    enterKey(e) {
        if (e.keyCode === 13 && !e.shiftKey) {
            this.props.sendMessage(this.refs.message);
            this.setState({ textChat: '' })
        }
    }

    updateTextChat(text) {
        this.setState({
            textChat: this.state.textChat + text
        });
    }

    handleClickSendMessage() {
        if(this.state.textChat==='')
            alert('Input message empty')
    }
    handleTextChange(event) {
        this.setState({ textChat: event.target.value })
    }
    render() {

        return (
            <div className="bottom_wrapper clearfix">
                <div className="message_input_wrapper">
                    <input
                        ref="message" className="message_input"
                        placeholder="Type your message here..."
                        onKeyUp={(e) => this.enterKey(e)}
                        onChange={this.handleTextChange.bind(this)}
                        value={this.state && this.state.textChat ? this.state.textChat : ''}
                    />
                </div>
                <div className="send_message"
                    onClick={() => {
                        this.props.sendMessage(this.refs.message);
                        this.setState({ textChat: '' });
                }}>
                    <div className="text" onClick={this.handleClickSendMessage.bind(this)} >Send</div>
                </div>
            </div>
        )
    }
}