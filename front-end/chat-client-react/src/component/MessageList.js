import React from 'react';
import ItemMessage from './ItemMessages'
import SocketConnection from '../SocketConnection';

export default class MessageList extends React.Component {
    render() {
        return (
            <div className="messages">
                <ul>
                    {this.props.messages.map((element, index) => {
                        return(
                            <li key={index} style={{listStyleType: 'none'}}>
                                <ItemMessage user={element.userId===SocketConnection.currentUser?true:false} userName={element.userId}
                                message={element.message}/>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}