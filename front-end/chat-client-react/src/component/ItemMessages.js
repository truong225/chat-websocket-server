import React from 'react';

export default class MessageItem extends React.Component {
    render() {
        return (
            <div className={this.props.user ? "message right appeared" : "message left appeared"}>
                <div className="avatar">
                    {this.props.userName}
                </div>
                <div className="text_wrapper">
                    <div className="text">
                        {this.props.message}
                    </div>
                </div>
            </div>
        )
    }
}